package msg

import (
    "fmt"
    "net/http"
    "io/ioutil"
    "strings"
    "os"
)

//The struct stores information about link and title of web page
type Link struct {
	Url string `json:"url"`
	Title string `json:"title"`
}

//The function returns Link pointer with appropriate url and title 
func getLink(url string) *Link {
	resp, err := http.Get(url)
    
	if err != nil {
        fmt.Fprintf(os.Stderr, "fetch: %v\n", err)
        os.Exit(1)
    }

    b, err := ioutil.ReadAll(resp.Body)
    resp.Body.Close()

    if err != nil {
        fmt.Fprintf(os.Stderr, "fetch: reading %s: %v\n", url, err)
        os.Exit(1)
    }
	
	return &Link{url, getTitle(string(b))}
}


//The function extracts title by parsing http response body
func getTitle(body string) string {
	start, end := strings.Index(body, TITLE_OPEN_TAG), strings.Index(body, TITLE_CLOSE_TAG)
	if start < 0 || end < 0 {
		return "no title"
	} else {
		return body[start + len(TITLE_OPEN_TAG) : end]
	}
}