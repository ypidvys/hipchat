package hipchat


var testCases = []struct {
	input string
	expected string
}{
	{
		"@chris you around?",
		"{\"mentions\":[\"chris\"]}",
	},

	{
		"Good morning! (megusta) (coffee)",
		"{\"emotions\":[\"coffee\",\"megusta\"]}",
	},

	{
		"Olympics are starting soon; http://www.nbcolympics.com",
		"{\"links\":[{\"url\":\"http://www.nbcolympics.com\",\"title\":\"2016 Rio Olympic Games | NBC Olympics\"}]}",
	},

	{
		"@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016",
		`{"mentions":["bob","john"],"emotions":["success"],"links":[{"url":"https://twitter.com/jdorfman/status/430511497475670016","title":"Justin Dorfman on Twitter: \u0026quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\u0026quot;"}]}`,
	},
}
