package msg

import (
   "unicode"
   "github.com/asaskevich/govalidator"
   "strings"
)


//The struct stores information about concrete type of special content
type SpecialContentType struct {
	prefix string
	extractSpecialContentValue func(string) (string, string, bool)
	addValueToSpecialContent func(sc *SpecialContent, value string)
}

const (
    MENTION string = "MENTION"
    EMOTION string = "EMOTION"
    HTTP_LINK string = "HTTP_LINK"
    HTTPS_LINK string = "HTTPS_LINK"
    MENTION_PREF string = "@"
    EMOTION_PREF string = "("
    HTTP_LINK_PREF string = "http://"
    HTTPS_LINK_PREF string = "https://"
    MAX_EMOTION_LENGTH = 15
)

//Enumeration of SpecialContentType entities 
var SpecialContentTypeList map[string]SpecialContentType =  map[string]SpecialContentType{
	MENTION: {MENTION_PREF, extractMention, addMention}, 
	EMOTION: {EMOTION_PREF, extractEmotion, addEmotion}, 
	HTTP_LINK: {HTTP_LINK_PREF, extractURL, addLink}, 
	HTTPS_LINK: {HTTPS_LINK_PREF, extractURL, addLink},
}



func (s SpecialContentType) getPrefix() string {
	return s.prefix
}


//The function extracts mention from beginning of the string and returns this mention, remainder string and success indicator
func extractMention(msg string) (mention string, remainder string, ok bool) {
	msg = msg[1:]
	for i, v := range msg {
		if !unicode.IsLetter(v) {
			return msg[:i], msg[i:], true
		} else if i == len(msg) - 1 {
			return msg[:], "", true
		}
	}

	return "", msg, false

}

//The function extracts emotion from beginning of the string and returns this emotion, remainder string and success indicator
func extractEmotion(msg string) (emotion string, remainder string, ok bool) {
	msg = msg[1:]
	for i, v := range msg {
		if v == ')' && i != 0 {
			return msg[:i], msg[i:], true
		} else if !unicode.IsLetter(v) || i > MAX_EMOTION_LENGTH {
			return "", msg, false
		}
	}
	return "", msg, false
}

//The function extracts url from beginning of the string and returns this url, remainder string and success indicator
func extractURL(msg string) (url string, remainder string, ok bool) {
	for i := len(msg); i > 0; i-- {
		if(govalidator.IsURL(msg[:i])) {
			return msg[:i], msg[i:], true
		}
	}

   return "", "", false
}

//check if current message starts with special content, returns special content type name
func startsWithSpecialContent(msg string) (string, bool) {
	for specialContentTypeName, v := range SpecialContentTypeList {
		if strings.HasPrefix(msg, v.getPrefix()) {
			return specialContentTypeName, true
		}
	}

	return "", false
}

//adds mention to special content struct
func addMention(sc *SpecialContent, mention string) {
	defer wg.Done()
	sc.Mentions = append(sc.Mentions, mention)
}

//adds emotion to special content struct
func addEmotion(sc *SpecialContent, emotion string) {
	defer wg.Done()
	sc.Emotions = append(sc.Emotions, emotion)
}

//adds link to special content struct
func addLink(sc *SpecialContent, url string) {
	defer wg.Done()
	sc.Links = append(sc.Links, getLink(url))
}