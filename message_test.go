package hipchat

import (
    "testing"
    "./msg"
)


func TestHipChatMessageInfo(t *testing.T) {
	var sc msg.SpecialContent 
	for _, test := range testCases {
		observed, _ := sc.Clean().Parse(test.input).ToJson()
	
		if (string(observed) != test.expected) {
			t.Fatalf("hcmi.Parse(%s).ToJson = %v, want %v", test.input, string(observed), test.expected)
		}
	}
}
