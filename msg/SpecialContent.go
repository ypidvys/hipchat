package msg

import (
    "encoding/json"
    "log"
    "sync"
)

const (
   TITLE_OPEN_TAG = "<title>"
   TITLE_CLOSE_TAG = "</title>"
)

//The struct stores a special content of HipChat message such as mentions, emotions and links.
type SpecialContent struct {
	Mentions []string `json:"mentions,omitempty"`
	Emotions []string `json:"emotions,omitempty"`
	Links []*Link `json:"links,omitempty"`
}

var wg sync.WaitGroup

//Function extracts special content from HipChat message and fill appropriate filds of SpecialContent struct
func (sc *SpecialContent) Parse(msg string) *SpecialContent {
	if len(msg) == 0 {
		wg.Wait()
		return sc
	}

	if specialContentTypeName, ok := startsWithSpecialContent(msg); ok {
		specialContentValue, remainder, ok := SpecialContentTypeList[specialContentTypeName].extractSpecialContentValue(msg); 
		if ok {
			wg.Add(1)
			go SpecialContentTypeList[specialContentTypeName].addValueToSpecialContent(sc, specialContentValue)
		} 
		sc.Parse(remainder)
	} else {
		sc.Parse(msg[1:])
	}

    return sc
}


func (sc *SpecialContent) ToJson() ([]byte, error) {
	data, err := json.Marshal(sc)
	if err != nil {
		log.Fatalf("JSON marshalling failed: %s", err)
	}
	return data, err
}


func (sc *SpecialContent) Clean() *SpecialContent {
	sc.Mentions = []string{}
	sc.Emotions = []string{}
	sc.Links = []*Link{}
	return sc
}